public class Tree {
    private String name;
    private char symbol;
    private int x;
    private int y;
    public Tree(String name, char symbol, int x, int y) {
        this.name = name;
        this.symbol = symbol;
        this.x = x;
        this.y = y;
    }
    public void print() {
        System.out.println(name + " x:" + x + " y:" + y);
    }
}
