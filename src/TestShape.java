public class TestShape {
    public static void main(String[] args) {
        Rectangle rect1 = new Rectangle("rect1", 10, 5);
        rect1.print();
        Rectangle rect2 = new Rectangle("rect2", 5, 3);
        rect2.print();

        Circle circle1 = new Circle("circle1", 1);
        circle1.print();
        Circle circle2 = new Circle("circle2", 2);
        circle2.print();

        Triangle tri1 = new Triangle("tri1", 5, 5, 6);
        tri1.print();
    }
}
